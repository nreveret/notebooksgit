Ce dossier contient des notebooks Jupyter.

Il s'agit de tutoriels interactifs permettant de découvrir des points de programmation et d'éxécuter et modifier les codes proposés.

Pour les lire vous pouvez :
- les télécharger sur votre disque dur et les ouvrir avec Jupyter Notepad. On pourra par exemple naviguer avec "anaconda prompt" (avec cd) jusqu'au dossier contenant les fichiers puis éxécuter la commande "jupyter notebook")
- cliquer sur le lien ci-dessous :

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/nreveret%2Fnotebooksgit/master)

cette dernière technique permet de les lire en ligne mais elle est un peu plus longue à mettre en place...