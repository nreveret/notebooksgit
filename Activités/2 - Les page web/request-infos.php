<!DOCTYPE html>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>GET headers</title>

</head>

<body style='width:95%;margin:auto;'>

    <h1 style='text-align:center;'>Les requêtes HTML</h1>

    <h2>La méthode reçue était du type <?=$_SERVER['REQUEST_METHOD']?></h2>

    <h3>Voici les informations passées dans l'entête de la requête</h3>

    <div style="padding:1%; border:solid 1px lightgray; background-color:lightgray;">
        <table>
            <?php
        $headers =  getallheaders();
    
        echo "<tr><td><b>Server Protocol :</td><td> " . $_SERVER['SERVER_PROTOCOL'] . "</td></tr>";
        
        foreach($headers as $key=>$val){
            echo "<tr><td><b>" . $key . ' :</b></td><td> ' . $val . '</td></tr>';
        }
    ?>
        </table>
    </div>

    <?php
        $datas = array();

    if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            $datas = $_GET;
        } else if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $datas = $_POST;
        }
    
        if (count($datas) > 0) :
    ?>
    <h3>Voici les données transmises par le client à l'aide de la requête <?=$_SERVER['REQUEST_METHOD']?></h3>

    <div style="padding:1%; border:solid 1px lightgray; background-color:lightgray;">
        <table>

            <?php foreach($datas as $key=>$val){
            echo "<tr>
                    <td><b>" . $key . ' :</b></td>
                    <td> ' . $val . '</td>
                </tr>';
            }
        else :
            echo "<p>Aucune données de type GET ou POST n'ont été transmises</p>";
        endif;
            ?>
        </table>
    </div>
</body>

</html>
