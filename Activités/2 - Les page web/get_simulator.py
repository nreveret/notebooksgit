import requests as rq

entete = {'User-agent': 'Mon Navigateur Perso !',
          'Accept' : 'Tous les fichiers !'}

requete = rq.get("https://request.urih.com/", headers = entete)

contenu_html = requete.content

with open('page.html', 'w') as file:  # Use file to refer to the file object

    file.write(contenu_html.decode())
