<!DOCTYPE html>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>GET headers</title>

</head>

<body style='width:95%;margin:auto;'>
    
    <h1 style='text-align:center;'>Les entêtes d'une requête</h1>
    
    <p1>Voici les informations passées dans l'entête de la requête</p>

    <?php
        $headers = getallheaders();
    
        foreach($headers as $key=>$val){
            echo "<b>" . $key . ' :</b> ' . $val . '<br><br>';
        }
    ?>

</body>

</html>
