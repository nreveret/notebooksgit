import requests as rq

entete = {'User-agent': 'Mon Navigateur Perso !',
          'Accept' : 'Tous les fichiers !'}

requete = rq.get("http://saint-francois-xavier.fr/nsi/request-infos.php", headers = entete)

html_doc = requete.content

with open('page.html', 'w') as file:
    file.write(html_doc.decode())
