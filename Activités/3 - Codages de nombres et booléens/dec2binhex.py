﻿# auteur : J.Rivet, le 06/07/2019 en Python 3.4
# conversion graphique automatique décimal - binaire ou décimal - hexadécimal
# Version 1 : ne traite que les entiers strictement positifs (inférieur à 2 puissance 24 sinon l'affichage dépasse)

import tkinter as tk
import tkinter.ttk as ttk
from tkinter import messagebox
from collections import namedtuple

# ---------------------- fonctions
def display_division(x, y, L, H, offset, quotient, diviseur, reste, showB):
    # place la boite élémentaire de division par 2

    barreV = display.create_line(x+L/2, y, x+L/2, y+H/2)
    barreH = display.create_line(x+L/2, y+H/2, x+L, y+H/2)
    tx_quotient = display.create_text(x + offset.qx, y + offset.qy ,text=str(quotient))
    tx_diviseur = display.create_text(x + offset.dx, y + offset.dy ,text=str(diviseur))
    tx_reste = display.create_text(x + offset.rx, y + offset.ry ,text=showB.format(reste).upper())

def convert(x, y, base, L,  H, offset):
    # convertit le nbre entier récupéré dans le champ de saisie (>0)
    # et affiche la suite de divisions par 2 graphiquement
    # affiche également la représentation binaire et le nbre de bits occupés

    try:
        nb_source = int(en_input.get())
    except:
        messagebox.showinfo("Alerte","il faut saisir un nombre entier positif")
        clear_text()
        en_input.focus_set()
        return

    if nb_source <= 0:
        messagebox.showinfo("Alerte","il faut saisir un nombre entier positif non nul")
        clear_text()
        en_input.focus_set()
        return

    display.create_rectangle(0,0,cv_largeur-1, cv_hauteur-1,fill="white") # efface l' ancien calcul
    nb_decimal = nb_source
    quotient = nb_decimal
    diviseur = base
    if base == 2:
        showB = "{0:b}"
    else:
        showB = "{0:x}"

    n = 0 #nombre de chiffres utilisés

    # affichage des divisions
    while quotient != 0:
        x += box_offsetX
        y += box_offsetY
        quotient = nb_decimal // diviseur
        reste = nb_decimal % diviseur
        #ch += str(reste)
        n += 1
        display_division(x, y, L, H, offset, nb_decimal, diviseur, reste, showB)
        nb_decimal = quotient

    if base == 2:
        lb_resultat["text"] = "  " + str(nb_source) + " (dec) = " + "{0:b}".format(nb_source) + " (bin)"
        n_octets = (n - 1) // 8 + 1
        lb_size["text"] = str(n) + " bits, " + str(n_octets) + " octets"
    else:
        lb_resultat["text"] = "  " + str(nb_source) + " (dec) = " + "{0:x}".format(nb_source).upper() + " (hex)"
        n_octets = (n - 1) // 2 + 1
        lb_size["text"] = str(n) + " chif, " + str(n_octets) + " octets"


def process(x,y, event=None):
    # appelée par le bt ok ou la touche entrée du clavier
    # lance la fonction de conversion
    base = tk_base.get()
    if base == 2:
        rb_bin.focus_set()
    else:
        rb_hex.focus_set()

    convert(x, y, base, L,  H, offset)

def clear_text(event=None):
    # efface le contenu de champ de saisie
    en_input.delete(0,largeur_champ)

# ---------------------- initialisation
x, y = 0, 0 # coord gauche, haut du cadre de l'opération (box)
L, H = 50, 50 #largeur, hauteur de du cadre
box_offsetX, box_offsetY = L/2 + 5, H/2 #décalage du cadre de l'opération

# offset contient les décalages en x et y des opérandes
# q pour nombre à diviser, d pour diviseur et r pour reste
Offset = namedtuple("offset",["qx", "qy", "dx", "dy", "rx", "ry"]) # crée une classe namedtuple
offset = Offset(qx=5, qy=10, dx=L/2 + 10, dy=10, rx=0, ry = H/2 + 10)
base = 2 # base 2 par défaut (possible 2 ou 16)

# ---------------------- GUI
appli =tk.Tk()

# placement fenetre tkinter
w_ecran = appli.winfo_screenwidth()
h_ecran = appli.winfo_screenheight()
largeur = 770
hauteur = 710
cv_largeur = largeur
cv_hauteur = hauteur - 66
margex, margey = 6, 10
res_largeur = 40
size_largeur = 14
largeur_champ = 8

bord_gauche, bord_haut = int((w_ecran - largeur) / 2), int((h_ecran - hauteur) / 2)
sizes = str(largeur) + "x" + str(hauteur) + "+" + str(bord_gauche) + "+" + str(bord_haut)
appli.geometry(sizes)
appli.title("Conversion décimal -binaire - hexadécimal - J.Rivet-2019")
appli['bg'] = "grey94"

tk_base = tk.IntVar() # variable pour les radio bouton choix de la base
tk_base.set(base)
# création des widgets
#cadre pour le menu
fr_saisie = tk.LabelFrame(appli, text="Saisie")
fr_saisie.grid(row=0, column=0, sticky=tk.W, padx = 10)

lb_input = tk.Label(fr_saisie, text="Taper un entier positif")
lb_input.grid(row=0, column=0, sticky=tk.W, padx=margex, pady=margey)

en_input = tk.Entry(fr_saisie, width=largeur_champ)
en_input.grid(row=0, column=1, sticky=tk.W, padx=margex, pady=margey)

bt_ok = ttk.Button(fr_saisie, text="Ok", width=3, command=lambda x=x, y=y : process(x, y))
bt_ok.grid(row=0, column=2, sticky=tk.W, padx=margex, pady=margey)

rb_bin = ttk.Radiobutton(fr_saisie,variable=tk_base, value=2, text="bin")
rb_bin.grid(row=0, column=3, sticky=tk.W, padx=margex, pady=margey)

rb_hex = ttk.Radiobutton(fr_saisie,variable=tk_base, value=16, text="hex")
rb_hex.grid(row=0, column=4, sticky=tk.W, padx=margex, pady=margey)

lb_resultat = tk.Label(fr_saisie, bg = "old lace", width=res_largeur)
lb_resultat.grid(row=0, column=5, sticky=tk.W, padx=margex, pady=margey)

lb_size = tk.Label(fr_saisie, bg = "old lace", width=size_largeur)
lb_size.grid(row=0, column=6, sticky=tk.W, padx=margex, pady=margey)

# création du canvas qui contient la suite de divisions
display = tk.Canvas(width=cv_largeur, height=cv_hauteur, bg="white")
display.grid(row=1, column=0)

#gestionnaire des événements bouton et clavier
en_input.bind("<Return>", lambda event, x=x, y=y: process(x, y, event))
en_input.bind("<FocusIn>", clear_text)

en_input.focus_set()

appli.mainloop()
