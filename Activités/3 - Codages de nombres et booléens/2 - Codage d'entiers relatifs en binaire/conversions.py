# Compléter les fonctions ci-dessous afin d'effectuer les différentes conversions

def dec_vers_bin(nombre) :
    """
    Fonction convertissant un nombre en binaire
    nombre est un entier positif ou nul
    Renvoie l'écriture binaire de nombre au format chaîne de caractère
    """

    # Vérification des préconditons
    assert isinstance(nombre, int), "nombre doit être un entier"
    assert nombre >= 0, "nombre doit être positif ou nul"

    # Code de la fonction
    
    # Retour (à modifier bien sûr)
    return None


def bin_vers_dec(binaire) :
    """
    Fonction convertissant un nombre du binaire à la base 10
    binaire est une chaîne de caractère non vide constituée uniquement de 0 et de 1
    Renvoie l'écriture en base 10 de binaire
    """

    # Vérification des préconditons
    assert isinstance(binaire, str), "binaire doit être une chaîne de caractères"
    assert len(binaire)> 0, "binaire doit être de longueur non nulle"
    assert all(c in '01' for c in binaire), "La chaîne ne doit être composée que de 0 et de 1"

    # Code de la fonction
    
    # Retour (à modifier bien sûr)
    return None


def dec_vers_hex(nombre) :
    """
    Fonction convertissant un nombre en hexadécimal
    nombre est un entier positif ou nul
    Renvoie l'écriture hexadécimale de nombre au format chaîne de caractère
    """

    # Vérification des préconditons
    assert isinstance(nombre, int), "nombre doit être un entier"
    assert nombre >= 0, "nombre doit être positif ou nul"

    # Code de la fonction
    
    # Retour (à modifier bien sûr)
    return None


def hex_vers_dec(hexa) :
    """
    Fonction convertissant un nombre de l'hexadécimal à la base 10
    binaire est une chaîne de caractère non vide constituée uniquement des caractères 0 à f
    Renvoie l'écriture en base 10 de hexa
    """

    # Vérification des préconditons
    assert isinstance(hexa, str), "hexa doit être une chaîne de caractères"
    assert len(hexa)> 0, "hexa doit être de longueur non nulle"
    assert all(c in '0123456789abcdedf' for c in hexa), "La chaîne ne doit être composée que de symboles de 0 à f"

    # Code de la fonction
    
    # Retour (à modifier bien sûr)
    return None


# Les tests pour vérifier que la fonction est bien codée.
# Coder une fonction puis ôter le # de la ligne correspondante afin de la tester

#assert dec_vers_bin(9) == "1001", "L'écriture binaire de 9 doit donner 1001"
#assert bin_vers_dec("1101") == 13, "Le binaire 1101 vaut 13"
#assert dec_vers_hex(64) == "40", "L'écriture hexadécimale de 64 doit donner 40"
#assert hex_vers_dec("4e") == 78, "L'hexadécimal 4e vaut 78"


# Utilisation des fonctions
#print(dev_vers_bin(9))
#print(bin_vers_dec("1001"))
#print(dev_vers_hex(64))
#print(hex_vers_dec("4e"))