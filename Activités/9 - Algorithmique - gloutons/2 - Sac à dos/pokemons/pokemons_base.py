def importation(url: str, separator: str) -> list:
    """
    Importe le fichier csv et renvoie les données sous forme d'une liste de dictionnaires
    :param url: l'adresse du fichier
    :param separator: le séparateur du fichier csv
    :return: la liste de dictionnaires contenant les données
    """
    result = []
    with open(url, "r") as f:
        lines = f.readlines()

    headers = lines[0].strip().split(separator)

    for line in lines[1:]:
        datas = line.strip().split(separator)
        dico = dict()
        for h, d in zip(headers, datas):
            if d.isnumeric():
                dico[h] = int(d)
            else:
                dico[h] = d
        result.append(dico)

    return result


def sort_by(datas: list, key: str, reversed=False) -> None:
    """
    Trie le tableau selon la colonne précisée en clé
    :param datas: la liste des dictionnaires
    :param key: la clé à utiliser en tri
    :param reversed: le tableau est-il classé dans l'ordre décroissant (False apr défaut)
    :return: None
    """
    datas.sort(key=lambda row: row[key], reverse=reversed)
