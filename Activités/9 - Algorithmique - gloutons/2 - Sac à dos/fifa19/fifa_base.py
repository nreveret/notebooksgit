def importation(url: str, separator: str) -> list:
    """
    Importe le fichier csv et renvoie les données sous forme d'une liste de dictionnaires
    :param url: l'adresse du fichier
    :param separator: le séparateur du fichier csv
    :return: la liste de dictionnaires contenant les données
    """
    result = []
    with open(url, "r", encoding="utf-8") as f:
        lines = f.readlines()

    headers = lines[0].strip().split(separator)

    for line in lines[1:]:
        datas = line.strip().split(separator)
        dico = dict()
        for h, d in zip(headers, datas):
            if h in ["Age","Overall","Potential","Value(K€)","Crossing","Finishing","HeadingAccuracy",
                     "ShortPassing","Volleys","Dribbling","Curve","FKAccuracy","LongPassing","BallControl",
                     "Acceleration","SprintSpeed","Agility","Reactions","Balance","ShotPower","Jumping",
                     "Stamina","Strength","LongShots","Aggression","Interceptions","Positioning",
                     "Vision","Penalties","Composure","Marking","StandingTackle","SlidingTackle","GKDiving",
                     "GKHandling","GKKicking","GKPositioning","GKReflexes"] :
                if d != "" :
                    dico[h] = float(d)
                else :
                    dico[h] = -1
            else:
                dico[h] = d
        result.append(dico)

    return result


def sort_by(datas: list, key: str, reversed=False) -> None:
    """
    Trie le tableau selon la colonne précisée en clé
    :param datas: la liste des dictionnaires
    :param key: la clé à utiliser en tri
    :param reversed: le tableau est-il classé dans l'ordre décroissant (False apr défaut)
    :return: None
    """
    datas.sort(key=lambda row: row[key], reverse=reversed)
