# -*- coding: utf-8 -*-
"""
Created on Wed Jan  1 11:23:31 2020

@author: rever
"""
from random import randint

l = [randint(-10**4,10**4) for k in range(10**5)]
with open("nombres.txt","w") as f :
    s = ""
    while l :
        s += str(l.pop()) + ","
    s = s[:-1]
    f.write(s)


