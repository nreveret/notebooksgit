from random import randint

taille = 10**6

with open("monstres.txt", "w") as f :
    for _ in range(taille) :
        f.write(str(randint(1,taille)) + "\n")