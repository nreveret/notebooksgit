# -*- coding: utf-8 -*-
"""
Created on Wed Jan  1 11:23:31 2020

@author: rever
"""
from random import randint

patients = 10000
vaccins = 10000

p = [randint(0,5000) for k in range(patients)]
with open("patients.txt","w") as f :
    s = ""
    while p :
        s += str(p.pop()) + ","
    s = s[:-1]
    f.write(s)


p = [randint(0,5000) for k in range(vaccins)]
with open("vaccins.txt","w") as f :
    s = ""
    while p :
        s += str(p.pop()) + ","
    s = s[:-1]
    f.write(s)


