from random import randint

taille = 10**4

with open("beau_nombre.txt", "w") as f :
    s = ""
    for _ in range(taille) :
        s += str(randint(1,taille)) + ","
    s = s[:-1]
    f.write(s)
