﻿# -*- coding: utf-8 -*-
"""
Created on Tue Jan  7 16:06:27 2020

@author: rever
"""

from time import perf_counter
from statistics import mean
from random import randint
from math import log10


def importe(url):
    """
    Import des données à trier à partir
    d'un fichier txt, les séparateurs sont des ","
    :param url: l'adresse du fichier
    :return: Renvoie une liste
    """
    A = []
    with open(url, "r") as f:
        A = f.readline().split(",")

    for i, elt in enumerate(A):
        A[i] = int(elt)

    return A


def chronometre(methode, liste, unit="ms", essais=10):
    """
    Choronomètre une méthode de tri passée en argument
    La liste est copiée avant chaque traitement
    :param methode: l'algorithme de tri
    :param liste:  la liste à trier
    :param unit: l'unité de temps : s, ms, µs ou ns
    :param essais: le nombre d'essais (tris) à reproduire
    :return: la moyenne du temps de tri
    """
    assert unit in ["s", "ms", "µs", "ns"], "L'unité est en s, ms, µs ou ns"

    temps = []

    for _ in range(essais):
        l_bis = liste[:]
        t_start = perf_counter()
        methode(l_bis)
        t_end = perf_counter()
        temps.append(t_end - t_start)

    retour = ""

    puissance = ["s", "ms", "µs", "ns"].index(unit)

    retour = "%.3f" % (mean(temps) * 1000 ** puissance) + " " + unit

    return retour + " (" + str(essais) + " essais)"


def chronometre_toute(liste, unit="ms", essais=10):
    """
    Chronomètre l'ensemble des méthodes de tri
    :param liste: la liste à trier
    :param unit: l'unité dans laquelle exprimer les résultats (s, ms, µs ou ns) en string
    :param essais: le nombre de répétitions
    :return: une chaîne reprenant les résutats
    """
    methodes = [selection, insertion, bulle, fusion, rapide, denombrement, base]

    s = ""

    for m in methodes:
        s += m.__name__ + " : \t" + chronometre(m, liste, unit, essais) + "\n"

    return s


def insertion(A):
    """
    Tri par insertion
    """
    i = 1
    while i < len(A):
        x = A[i]
        j = i - 1
        while j >= 0 and A[j] > x:
            A[j + 1] = A[j]
            j = j - 1
        A[j + 1] = x
        i = i + 1

    return A


def selection(A):
    """
    Tri par sélection
    """
    for i in range(len(A) - 1):
        i_mini = i
        mini = A[i_mini]
        for j in range(i + 1, len(A)):
            if A[j] < mini:
                i_mini = j
                mini = A[i_mini]
        A[i], A[i_mini] = A[i_mini], A[i]

    return A


def denombrement(A):
    """
    Tri par dénombrement
    Le maximum de la liste ne dépasse pas la longueur de la liste
    """

    E = [0] * (len(A) + 1)

    for elt in A:
        E[elt] = E[elt] + 1

    A_retour = []

    for idx, elt in enumerate(E):
        A_retour += ([idx] * elt)

    return A_retour


def bulle(A):
    """
    Tri à bulle
    """
    n = len(A)
    swap = True
    while swap:
        swap = False
        for i in range(1, n):
            if A[i - 1] > A[i]:
                A[i - 1], A[i] = A[i], A[i - 1]
                swap = True
        n = n - 1

    return A


def fusion(A):
    longueur = len(A)
    if longueur == 1:
        return A
    else:
        debut = fusion(A[:longueur // 2])
        fin = fusion(A[longueur // 2:])

        retour = []

        while (debut and fin):
            if debut[0] < fin[0]:
                retour.append(debut.pop(0))
            else:
                retour.append(fin.pop(0))
        while debut:
            retour.append(debut.pop(0))
        while fin:
            retour.append(fin.pop(0))

    return retour


def partition(T, debut, fin, pivot):
    """
    Partition associée au tri rapide
    """
    j = debut
    for i in range(debut, fin - 1):
        if T[i] < pivot:
            T[i], T[j] = T[j], T[i]
            j += 1
    T[j], T[fin] = T[fin], T[j]

    return j


def rapide(A, debut=0, fin=-1):
    """
    Tri rapide
    """
    if fin == -1:
        fin = len(A) - 1
    if debut < fin:
        # Choix du pivot
        i_pivot = randint(debut, fin - 1)
        A[i_pivot], A[-1] = A[-1], A[i_pivot]
        pivot = A[-1]

        pivot = partition(A, debut, fin, pivot)
        rapide(A, debut, pivot - 1)
        rapide(A, pivot + 1, fin)

    return A

def base(A, taille_paquet = 1) :
    """
    Tri par base (radix)
    On utilise la décomposition selon la base 10
    et le tri par dénombrement
    :param A: la liste à trier
    :return: la liste triée
    """
    # Nombre de chiffres dans les nombres
    taille = (int(log10(max(A))))//2 + 1
    # On traite tous les chiffres
    for i in range(0, taille) :
        # Les valeurs des chiffres de 10**i
        decimales = [[] for k in range(10**taille_paquet)]
        for elt in A :
            decimale = (elt // 10**(taille_paquet*i)) % (10 ** taille_paquet)
            decimales[decimale].append(elt)
        # On crée la nouvelle liste
        A_retour = []
        for elt in decimales :
            for e in elt :
                A_retour.append(e)
        A = A_retour[:]

    return A

if __name__ == "__main__" :
    A = importe("10000.txt")
    print(chronometre_toute(A))
