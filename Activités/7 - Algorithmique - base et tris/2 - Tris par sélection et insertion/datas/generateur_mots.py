# -*- coding: utf-8 -*-
"""
Created on Wed Jan  1 11:23:31 2020

@author: rever
"""
from random import sample

alphabet="abcdefghijklmonpqrstuvwxyz"

n = 5

for i in range(1,n+1) :
    l = [sample(alphabet, 6) for k in range(10**i)]
    with open(str(10**i)+"_mots.txt","w") as f :
        s = ""
        while l :
            s += "".join(l.pop()) + ","
        s = s[:-1]
        f.write(s)


