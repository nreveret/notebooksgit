# -*- coding: utf-8 -*-
"""
Created on Wed Jan  1 11:23:31 2020

@author: rever
"""
from random import randint

n = 6

for i in range(1,n+1) :
    l = [randint(0,10**i) for k in range(10**i)]
    with open(str(10**i)+".txt","w") as f :
        s = ""
        while l :
            s += str(l.pop()) + ","
        s = s[:-1]
        f.write(s)


