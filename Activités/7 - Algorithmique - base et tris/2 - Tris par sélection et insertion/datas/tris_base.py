# -*- coding: utf-8 -*-
"""
Created on Tue Jan  7 16:06:27 2020

@author: rever
"""

from time import perf_counter_ns
from statistics import mean


def importe(url) :
    """
    Import des données à trier à partir
    d'un fichier txt, les séparateurs sont des ","
    Renvoie une liste
    """
    A = []
    with open(url, "r") as f :
        A = f.readline().split(",")
    
    for i, elt in enumerate(A):
        A[i] = int(elt)
    
    return A

def chronometre(methode, liste, unit = "ms", essais = 10) :
    assert unit in ["s", "ms", "µs", "ns"], "L'unité est en s, ms, µs ou ns"
    
    temps = []
    
    for _ in range(essais) :
        l_bis = liste[:]
        t_start = perf_counter_ns()
        methode(l_bis)
        t_end = perf_counter_ns()
        temps.append(t_end - t_start)
    
    retour = ""
    
    puissance = ["s", "ms", "µs", "ns"].index(unit)
    
    retour = "%.3f" % (mean(temps) /10**9 * 1000**puissance) + " " + unit
    
    
    return retour + " (" + str(essais) + " essais)"
