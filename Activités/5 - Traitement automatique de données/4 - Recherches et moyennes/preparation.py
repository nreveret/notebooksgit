def import_datas(adresse) :
    tab = []

    with open(adresse, "r", encoding="utf-8") as f :
        for ligne in f :
            tab.append(ligne[:-1].split(","))
    
    tab.pop(0)
    
    return tab

def type_datas(tab) :
    for ligne in tab :
        ligne[0] = int(ligne[0])
        for i in [-1,-3] :
            if ligne[i] != "?" :
                ligne[i] = int(ligne[i])
    
    return tab


def renvoie_non_vide(tab) :
    return [ligne for ligne in tab if ligne[-3] != "?" and ligne[-1] != "?"]

def search(source, condition) :
    tab = []
    
    return [l for l in source if eval(condition)]

heroes = import_datas("marvel.csv")

heroes = type_datas(heroes)

heroes_bis = renvoie_non_vide(heroes)
