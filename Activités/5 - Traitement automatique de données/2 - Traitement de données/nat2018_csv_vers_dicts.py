# -*- coding: utf-8 -*-
"""
Created on Wed Oct 23 08:42:56 2019

@author: rever
"""
dico_prenoms = []

with open("nat2018.csv", "r", encoding="utf-8-sig") as fichier :
    entetes = fichier.readline()[:-1].split(";")
    for ligne in fichier :
        valeurs = ligne[:-1].split(";")
        dico = {}
        for c, v in zip(entetes, valeurs):
            if c in ['annee_naissance', 'nombre']  :
                v = int(v)
            dico[c] = v
        dico_prenoms.append(dico)


antoines = [p for p in dico_prenoms if p['prenom'] == 'ANTOINE' and p['sexe'] == 'M' ]

print(sorted(antoines, key = lambda annee : annee['nombre']))
