# -*- coding: utf-8 -*-
"""
Created on Wed Oct 23 08:42:56 2019

@author: rever
"""

#Génération d'un liste des élèves (sans les entêtes des données)
eleves = []

with open("premieres.csv", "r", encoding="utf-8-sig") as fichier :
    fichier.readline() #la 1ère ligne du fichier contient les entêtes, on l'ignore
    for ligne in fichier :
        eleves.append(ligne[:-1].split(",")) #le -1 pour éviter le "\n" de fin de ligne


print(eleves)

dico_eleves = []

with open("premieres.csv", "r", encoding="utf-8-sig") as fichier :
    entetes = fichier.readline()[:-1].split(",")
    for ligne in fichier :
        valeurs = ligne[:-1].split(",")
        dico = {}
        for c, v in zip(entetes, valeurs):
            dico[c] = v
        dico_eleves.append(dico)

print(dico_eleves)

# Combien d'élèves ont pris NSI ? (sur le créneau de spé B chez nous)
print(len([eleve for eleve in dico_eleves if "NSI" in eleve['Spé B']]))
