# -*- coding: utf-8 -*-
"""
Created on Wed Oct 23 08:42:56 2019

@author: rever
"""
prenoms = []

with open("nat2018.csv", "r", encoding="utf-8-sig") as fichier :
    for ligne in fichier :
        prenoms.append(ligne[:-1].split(";"))

#print(prenoms)

prenoms.pop(0)

#print(prenoms[0])

for ligne in prenoms :
    ligne[2] = int (ligne[2])
    ligne[3] = int (ligne[3])

#print(prenoms[0])

#prenoms_masc = [ligne for ligne in prenoms if ligne[0] == 'M']
#prenoms_fem = [ligne for ligne in prenoms if ligne[0] == 'F']

camille_fille = [ligne for ligne in prenoms if ligne[1] == 'CAMILLE' and ligne[0] == 'F']
camille_garcon = [ligne for ligne in prenoms if ligne[1] == 'CAMILLE' and ligne[0] == 'M']

import matplotlib.pyplot as plt

annees = [ligne[2] for ligne in camille_fille]
nombres = [ligne[3] for ligne in camille_fille]

plt.bar(annees, nombres)

annees = [ligne[2] for ligne in camille_garcon]
nombres = [ligne[3] for ligne in camille_garcon]

plt.bar(annees, nombres)
plt.show()


#print(nicolas)
