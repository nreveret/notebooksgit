# -*- coding: utf-8 -*-
"""
Created on Wed Oct 23 08:42:56 2019

@author: rever
"""

#Génération d'un liste des élèves (sans les entêtes des données)
eleves = []

with open("premieres.csv", "r", encoding="utf-8-sig") as fichier :
    for ligne in fichier :
        eleves.append(ligne[:-1].split(",")) #le -1 pour éviter le "\n" de fin de ligne

eleves.pop(0)

for eleve in eleves :
	eleve.append( "".join(sorted( [ eleve[4], eleve[5], eleve[6] ] )) ) 

triplets = {}

for eleve in eleves :
    if eleve[11] in triplets.keys() :
        triplets[eleve[11]] += 1
    else :
        triplets[eleve[11]] = 1

tab_triplets = [(k,v) for (k,v) in zip(triplets.keys(), triplets.values())]

tab_triplets = sorted(tab_triplets, key = lambda tr : tr[1], reverse = True)

import matplotlib.pyplot as plt

plt.bar([t[0] for t in tab_triplets], [t[1] for t in tab_triplets])
plt.title("Triplets pris")
plt.xticks(rotation='vertical')
plt.show()