<!doctype html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <title>Base de php</title>
</head>

<body>
    <h1>Bases de PHP</h1>

    <p>Ceci est du texte en html</p>

    <?php

    // Ici commence le code php

    // Un premier affichage de texte avec echo
    echo "<p></p>Ceci est du texte généré par php !</p>";

    // On définit notre créneau horaire
    date_default_timezone_set('Europe/Paris');

    // Un second affichage
    echo "<p>Cela permet par exemple d'afficher l'heure exacte : " . date("H:i:s") . "</p>";

    // Un test sur la parité de la seconde de l'heure
    $seconde = date("s");
    if ($seconde % 2 == 0) {
        echo "<p>La seconde est paire !</p>";
    } else {
        echo "<p>La seconde est impaire !</p>";
    }

    // Un boucle Pour
    $ligne = "";
    for ($i = 0; $i < 10; $i++) {
        $ligne = $ligne . "--";
        echo $ligne . "<br>";
    }

    // Un Tant que, substr est la fonction substring (sous-chaîne): on garde la sous-chaîne allant du caractère 0 au -2 (avant, avant dernier)
    while ($ligne != "") {
        $ligne = substr($ligne, 0, -2);
        echo $ligne . "<br>";
    }

    ?>

    <!--Retour au html pur, ceci est un commentaire -->

    <footer>
        Ceci est le pied de page en html
    </footer>
</body>

</html>