import PIL.Image, PIL.ExifTags
img = PIL.Image.open('1.jpg')

exif = {
    PIL.ExifTags.TAGS[k]: v
    for k, v in img._getexif().items()
    if k in PIL.ExifTags.TAGS
}