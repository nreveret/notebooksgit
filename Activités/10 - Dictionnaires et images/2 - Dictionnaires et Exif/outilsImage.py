"""
Ce module contient différentes fonctions facilitant le projet 'Histoire Codée'
Il requiert l'import suivant :\n
- PIL
"""

from PIL.ExifTags import TAGS, GPSTAGS


def to_deg_min_sec(value, loc):
    """
    Convertit une donnée en décimal en degré/minutes/secondes

    Args:
        value: la valeur en décimal
        loc: un tableau contenant soit ['S','N'] soit ['W','E']

    Returns:
        un tuble avec les valeurs entières en degré / minutes / secondes et la localisation
    """
    if value < 0:
        loc_value = loc[0]
    elif value > 0:
        loc_value = loc[1]
    else:
        loc_value = ""
    abs_value = abs(value)
    deg = int(abs_value)
    t1 = (abs_value - deg) * 60
    min = int(t1)
    sec = round((t1 - min) * 60, 5)
    return deg, min, sec, loc_value


def get_exif_data(image):
    """
    Récupère les données EXIF d'une image et les renvoie sous forme d'un dictionnaire

    Args:
        image: l'image PIL

    Returns:
        un dictionnaire contenant les données EXIF
    """
    exif_data = {}
    info = image._getexif()
    if info:
        for tag, value in info.items():
            decoded = TAGS.get(tag, tag)
            if decoded == "GPSInfo":
                gps_data = {}
                for t in value:
                    sub_decoded = GPSTAGS.get(t, t)
                    gps_data[sub_decoded] = value[t]

                exif_data[decoded] = gps_data
            else:
                exif_data[decoded] = value

    return exif_data


def get_if_exist(data, key):
    """
    Renvoie la valeur associée à une clé de dictionnaire, None si la clé n'existe pas.
    Cette fonction n'est *a priori* pas à utiliser hors de ce module

    Args:
        data: le dictionnaire
        key: la clé
    Returns:
        la valeur ou None
    """
    if key in data:
        return data[key]

    return None


def to_decimal_deg(value):
    """
    Convertit une données GPS en degrés décimaux
    Les données de localisation GPS sont données sous forme de triplet de couples :
    (Degré, coefficient Degré) (Minute, coefficient Minute) (Seconde, coefficient Seconde)
    On calcule la valeur en degré décimaux en faisant D * coeffD + (M * coeffM) / 60 + (S * coeffS) / 60^2
    L'intérêt des coefficients est principalement sur les secondes : allouer un coefficient de 10000
    permet de stocker la valeur de seconde sous forme entière sans perdre de précision

    Args:
        value: Un tuple contenant les trois couples de coordonnées
    Returns:
        la valeur en degré en décimal
    """
    d0 = value[0][0]
    d1 = value[0][1]
    d = float(d0) / float(d1)

    m0 = value[1][0]
    m1 = value[1][1]
    m = float(m0) / float(m1)

    s0 = value[2][0]
    s1 = value[2][1]
    s = float(s0) / float(s1)

    return d + (m / 60.0) + (s / 3600.0)


def get_lat_lon(exif_data):
    """
    Renvoie la latitude et la longitude (en décimaux) extraite de données GPS dans les données EXIF
    Args:
        exif_data: données EXIF d'une photo sous forme d'un dictionnaire tel que généré par la fonction get_exif_data
    Returns:
        un couple de flottants (latitude, longitude)
    """
    lat = None
    lon = None

    if "GPSInfo" in exif_data:
        gps_info = exif_data["GPSInfo"]
        gps_latitude = get_if_exist(gps_info, "GPSLatitude")
        gps_latitude_ref = get_if_exist(gps_info, "GPSLatitudeRef")
        gps_longitude = get_if_exist(gps_info, "GPSLongitude")
        gps_longitude_ref = get_if_exist(gps_info, "GPSLongitudeRef")

        if gps_latitude and gps_latitude_ref and gps_longitude and gps_longitude_ref:
            lat = to_decimal_deg(gps_latitude)
            if gps_latitude_ref != "N":
                lat = 0 - lat

            lon = to_decimal_deg(gps_longitude)
            if gps_longitude_ref != "E":
                lon = 0 - lon

    return lat, lon
