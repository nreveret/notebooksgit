# importation le module
import folium

# création d'une carte. On donne en argument :
# -> La localisation à l'aide des coorodnnées GPS
# -> le degré de zoom initial (facultatif)
# D'autres options sont disponibles. Consulter la documentation
carte = folium.Map(location=[48.85327, 2.34886], zoom_start=10)


# Ajouter une punaise
folium.Marker([48.11137, -1.67978], popup='<i>Mairie de Rennes</i>', tooltip="Clique moi !").add_to(carte)


# Sauvegarder la carte au format html
carte.save('carte.html')

