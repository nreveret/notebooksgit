# -*- coding: utf-8 -*-

import socket

def ouverture(ip = "localhost", port = 60000) :
    """
    Fonction ouvrant un socket vers l'IP et le port indiqués
    ip est l'IP visée au format string. Par défaut "localhost"
    port est le Port visé au format int. Par défaut 60000
    Renvoie le socket ainsi créé

    S'emploie par exemple dans la console (sans arguments donc
    vers ("localhost", 60000)):

    >>> client = ouverture()

    Ou (vers "10.10.25.2", 62000) :
    >>> client = ouverture("10.10.25.2", 62000)
    """
    client = socket.socket()
    client.connect((ip,port))

    return client


def envoi(c, message) :
    """
    Fonction envoyant le message passé en argument sur le socket c
    Le texte est envoyé encodé (sous forme d'octets)
    c est un socket de communication déjà ouvert
    message est une chaîne de caractère
    Renvoie None

    S'emploie par exemple dans la console :

    >>> envoi(c, "coucou")
    """
    c.send(message.encode())

    return None

def recoit(c) :
    """
    Fonction écoutant et recavant un message sur le socket c
    Cette fonction est bloquante : tant que'aucun message n'est reçu
    elle ne retourne pas de valeur
    c est un socket de communication déjà ouvert
    Renvoie le message reçu décodé (au format string)

    S'emploie par exemple dans la console :

    >>> message = recoit(c)
    """

    m = c.recv(255)

    return m.decode()


def ferme(c) :
    """
    Fonction fermant le socket c
    c est un socket de communication déjà ouvert

    Renvoie None

    S'emploie par exemple dans la console :

    >>> ferme(c)
    """

    c.close()

    return None
