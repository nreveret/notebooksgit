import tkinter as tk
import threading
import socket


class ThreadSocket(threading.Thread):
    """
    Classe gérant la communication
    Elle hérite de threading.Thread ce qui lui permet de s'exécuter de façon indépendante de la fenêtre
    """

    def __init__(self, port):
        """
        Constructeur
        :param port: port du serveur 
        """
        threading.Thread.__init__(self)
        self.port = port
        self.connexion_succes = True
        self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.ip = "localhost"
        self.message = ""
        self.closing = False
        self.send_token = False

    def run(self):
        """
        Lancement de la thread (avec thread.start())
        Gère la connexion avec le serveur (et les erreurs) puis lance la boucle gérant les échanges (loop)
        :return: None
        """
        self.client.settimeout(10)
        try:
            self.client.connect((self.ip, self.port))
        except socket.error as exc:
            erreur_connexion(exc)
            self.connexion_succes = False
            print("error")
        if self.connexion_succes:
            self.loop()

    def loop(self):
        """
        Boucle gérant les échanges (alternativement réception puis envoi)
        :return: None
        """
        self.client.settimeout(600)
        affichage_connexion()
        while not self.closing:
            self.message = ""
            while self.message == "":
                self.message = self.client.recv(255)
            print("thread reception")
            reception(self.message)
            while not self.send_token:
                pass
            self.client.send(self.message.encode())
            print("thread envoi")
            self.message = ""
            self.send_token = False
        print("fenetre fermée")


def erreur_connexion(m):
    """
    Affiche le contenu des erreurs dans la fenêtre
    :param m: L'erreur renvoyée par Python
    :return: None
    """
    text.config(state=tk.NORMAL)
    text.delete(1.0, tk.END)
    text.insert(tk.END, "Erreur de connexion : %s" % m)
    text.config(state=tk.DISABLED)


def connexion():
    """
    Fonction exécutée lors du 1er clic sur le bouton
    Met à jour l'ip du serveur de la thread puis la démarre
    :return: None
    """
    thread.ip = entry.get()
    thread.start()
    button.config(state=tk.DISABLED)
    text.config(state=tk.NORMAL)
    text.delete(1.0, tk.END)
    text.insert(tk.END, "Attente de connexion")
    text.config(state=tk.DISABLED)


def affichage_connexion():
    """
    Informe l'utilisateur de la connexion par le biais de la fenêtre
    :return: None
    """
    text.config(state=tk.NORMAL)
    text.delete(1.0, tk.END)
    text.insert(tk.END, "Connexion établie avec " + thread.ip + "\n")
    text.config(state=tk.DISABLED)
    button.config(text="Envoi", command=envoi)
    entry.bind("<Return>", text_return)


def text_return(event):
    """
        Permet d'envoyer un message à l'aide de la frappe de la touche "entrée" à la fin de la saisie
        :param event: l'évènement généré par la frappe
        :return: None
    """
    envoi()


def envoi():
    """
    Fonction appelée par le bouton d'envoi de la fenêtre
    Récupère le contenu de la zone de saisie et gère le cas où elle est vide.
    Si le message existe, saisie non vide, un jeton d'envoi (token) est activé
    La thread le repère et envoi le message avant de d&sactiver le jeton
    :return: 0 si la zone de saisie est vide, None sinon
    """
    thread.message = entry.get()
    if thread.message == "":
        return 0
    text.config(state=tk.NORMAL)
    entry.delete(0, tk.END)
    text.insert(tk.END, "Serveur : " + thread.message + "\n")
    text.config(state=tk.DISABLED)
    button.config(state=tk.DISABLED)
    thread.send_token = True


def reception(m):
    """
    Affiche le message reçu par la thread
    Cette fonction est appelée par la thread lorsqu'elle reçoit un message
    :param m: message reçu par le socket
    :return: None
    """
    text.config(state=tk.NORMAL)
    text.insert(tk.END, "Client : " + m.decode() + "\n")
    text.config(state=tk.DISABLED)
    button.config(state=tk.NORMAL)


# Création de la fenêtre
root = tk.Tk()
root.config(width=200, height=400)
root.title("Client")

# Zone d'affichage des messages et barre de défilement
scrollbar = tk.Scrollbar(root)
scrollbar.grid(row=0, column=2, sticky="ns")
text = tk.Text(root, yscrollcommand=scrollbar.set)
text.grid(row=0, column=0, columnspan=2)
text.insert(tk.END, "Cliquer sur le bouton pour vous connecter")
text.config(state=tk.DISABLED)
scrollbar.config(command=text.yview)

# Zone de saisie
entry = tk.Entry(root, width=40)
entry.insert(0, "localhost")
entry.grid(row=1, column=0)

# Bouton de connexion et d'envoi des messages
button = tk.Button(root, text="Connexion", command=connexion)
button.grid(row=1, column=1)

# Création de la thread
thread = ThreadSocket(60000)

# Lancement de la fenêtre
root.mainloop()

# Fermeture du socket client lorsque la fenêtre est fermée
thread.closing = True
thread.client.close()
