# -*- coding: utf-8 -*-
"""
Created on Thu Dec  5 17:55:36 2019

@author: rever
"""
import socket

def ouverture(port = 60000):
    """
    Fonction ouvrant un socket-server sur le poste au port passé en argument
    et attend la connexion du client
    Cette fonction est bloquante, tant que la connexion avec le client
    n'est pas établie, cette fonction ne renvoie rien

    port est le Port visé au format int. Par défaut 60000

    Renvoie le socket-server ainsi créé, le socket de communication
    vers le client et les informations du client (IP, port)

    S'emploie par exemple dans la console (sans arguments donc
    sur le port 60000):

    >>> s_server, client, infos = ouverture()

    Ou (sur le port 62000) :
    >>> s_server, client, infos = ouverture(62000)
    """
    sock = socket.socket()
    sock.bind(('',60000))
    sock.listen(1)
    client, infos = sock.accept()

    return sock, client, infos

def envoi(c, message) :
    """
    Fonction envoyant le message passé en argument sur le socket c
    Le texte est envoyé encodé (sous forme d'octets)
    c est un socket de communication déjà ouvert
    message est une chaîne de caractère
    Renvoie None

    S'emploie par exemple dans la console :

    >>> envoi(c, "coucou")
    """

    c.send(message.encode())

    return None

def recoit(c) :
    """
    Fonction écoutant et recavant un message sur le socket c
    Cette fonction est bloquante : tant que'aucun message n'est reçu
    elle ne retourne pas de valeur
    c est un socket de communication déjà ouvert
    Renvoie le message reçu décodé (au format string)

    S'emploie par exemple dans la console :

    >>> message = recoit(c)
    """

    m = c.recv(255)

    return m.decode()

def ferme(c, s) :
    """
    Fonction fermant le socket c
    c est un socket de communication déjà ouvert

    Renvoie None

    S'emploie par exemple dans la console :

    >>> ferme(c)
    """

    c.close()
    s.close()

    return None
