# -*- coding: utf-8 -*-
"""
Created on Sat Dec  7 10:06:51 2019

@author: rever
"""


def byte_to_text_file(message, nom_fichier) :
    
    nombres = [ord(caractere) for caractere in message]
    
    octets = [hex(nombre) for nombre in nombres]
    
    with open(nom_fichier, 'wb') as f :
        for nombre in nombres :
            f.write(bytes([nombre]))
    
    return nombres, octets