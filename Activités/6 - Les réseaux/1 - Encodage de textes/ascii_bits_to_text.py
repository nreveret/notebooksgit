﻿def ascii7_to_text(bits) :
    """
    Fonction convertissant une chaîne de bits en caractères
    On utilise le codage de caractère ASCII sur 7 bits

    bits est une chaîne de bits comptant 7*N bits (N étant le nombre de
    caractère en sortie)

    Renvoie le texte converti
    """

    assert len(bits) % 7 == 0, "La chaîne de bits doit être un multiple de 7"
    for bit in bits :
        assert bit in ["0", "1"], 'Chaque bit doit être un "0" ou un "1"'

    message = ""

    paquets = [bits[i:i+7] for i in range(0,len(bits),7)]

    for paquet in paquets :
        message += chr(int(paquet,2))

    return message


def ascii8_to_text(bits) :
    """
    Fonction convertissant une chaîne de bits en caractères
    On utilise le codage de caractère ASCII sur 8 bits

    bits est une chaîne de bits comptant 8*N bits (N étant le nombre de
    caractère en sortie)

    Renvoie le texte converti
    """

    assert len(bits) % 8 == 0, "La chaîne de bits doit être un multiple de 8"
    for bit in bits :
        assert bit in ["0", "1"], 'Chaque bit doit être un "0" ou un "1"'

    message = ""

    paquets = [bits[i:i+8] for i in range(0,len(bits),8)]

    for paquet in paquets :
        message += chr(int(paquet,2))

    return message


def unicode16_to_text(bits) :
    """
    Fonction convertissant une chaîne de bits en caractères
    On utilise le codage de caractère ASCII sur 8 bits

    bits est une chaîne de bits comptant 16*N bits (N étant le nombre de
    caractère en sortie)

    Renvoie le texte converti
    """

    assert len(bits) % 16 == 0, "La chaîne de bits doit être un multiple de 16"
    for bit in bits :
        assert bit in ["0", "1"], 'Chaque bit doit être un "0" ou un "1"'

    message = ""

    paquets = [bits[i:i+16] for i in range(0,len(bits),16)]

    for paquet in paquets :
        message += chr(int(paquet,2))

    return message


def text_to_ascii7(texte) :
    """
    Fonction convertissant un texte en ASCII sur 7 bits
    texte est le texte à saisir. Il ne doit contenir que les caractères
    de la table ASCII initiale (codés sur 7 bits)
    Renvoie le code binaire du texte sous forme de string
    """
    
    for caractere in texte :
        assert ord(caractere) < 128, "Tous les caractères doivent être dans la table ASCII"
    
    code = ""
    
    for caractere in texte :
        nb = ord(caractere)
        binaire = bin(nb)[2:].zfill(7)
        print(len(binaire))
        code += binaire
    
    return code


def text_to_ascii8(texte) :
    """
    Fonction convertissant un texte en ASCII sur 8 bits
    texte est le texte à saisir. Il ne doit contenir que les caractères
    de la table ASCII initiale (codés sur 8 bits)
    Renvoie le code binaire du texte sous forme de string
    """
    
    for caractere in texte :
        assert ord(caractere) < 255, "Tous les caractères doivent être dans la table ASCII étendue"
    
    code = ""
    
    for caractere in texte :
        nb = ord(caractere)
        binaire = bin(nb)[2:].zfill(8)
        print(len(binaire))
        code += binaire
    
    return code