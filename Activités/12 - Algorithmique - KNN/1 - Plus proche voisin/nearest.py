import matplotlib.pyplot as plt
from random import randint
from math import sqrt

def distance(point1, point2) :
    return sqrt((point1[0]-point2[0])**2+(point1[1]-point2[1])**2)

nb_points = 20
points = [(randint(-10, 10), randint(-10,10)) for i in range(nb_points)]
distances = [(i, distance((0,0), points[i])) for i in range(nb_points)]
distances.sort(key = lambda x : x[1])

x = [point[0] for point in points]
y = [point[1] for point in points]


for point in points :
    plt.plot([0, point[0]],[0, point[1]], "b:")

plt.plot([0, points[distances[0][0]][0]],[0, points[distances[0][0]][1]], "g-", linewidth=5)
plt.plot(0,0, "ro", markersize = 10)
plt.plot(x,y, "bo")


plt.show()