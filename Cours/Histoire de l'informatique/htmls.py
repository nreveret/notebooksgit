debut = """
<!DOCTYPE HTML>

<html>

<head>
    <title>Histoire de l'Informatique</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="css/style.css" />
</head>

<body>
    <div id="content">
    <header>
        <h1>&#201;léments d'histoire de l'informatique</h1>

        <p>Ce document ne vise pas à être exhaustif. Seules certaines dates et personnages importants ou amusants sont retenus</p>
        
        <p>On pourra en complément consulter les ressources suivantes :</p>
        
        <ul>
            <li>Ce <a href="https://histoire-informatique.org/">site</a> très clair</li>
            <li>Ce <a href="http://www.e-miage.fr/MONE2/section1/pdf/section1_1.pdf">pdf</a> bien illustré</li>
            <li>Ce <a href="https://delmas-rigoutsos.nom.fr/documents/YDelmas-histoire_informatique.pdf">pdf</a> très complet</li>
            <li>Différentes illustrations (en anglais) :</li>
            <ul>
                <li><a href="https://i.pinimg.com/564x/a7/fd/24/a7fd245247f84d867fc9707bb6fbb430.jpg">Histoire des ordinateurs</a></li>
                <li><a href="https://i.pinimg.com/736x/49/d5/66/49d5669cc6ce8a2ec5a461ae32d52f21.jpg">Histoire des processeurs</a></li>
                <li><a href="https://i.pinimg.com/736x/80/4a/54/804a54d3e6f6c2604fc28f677eede5d7--internet-history-timeline-infographic.jpg">Histoire d'internet</a></li>
            </ul>
        </ul>
        </header>
"""

gauche = """
        <div class="gauche">
            <aside>
                <a href="%s">
                    <h2>
                        %s
                    </h2>
                </a>
                <div class="date">
                    %s
                </div>
                %s
            </aside>
            <figure>
                <img src="%s" alt="%s">
                <figcaption>%s</figcaption>
            </figure>
        </div>
"""

droite = """
<div class="droite">
            <figure>
                <img src="%s" alt="%s">
                <figcaption>%s</figcaption>
            </figure>
            <aside>
                <a href="%s">
                    <h2>
                        %s
                    </h2>
                </a>
                <div class="date">
                    %s
                </div>
                %s
            </aside>
        </div>
"""

fin = """

    <footer>
    N. Revéret
    </footer>
    </div>

</body>

</html>
"""
