# -*- coding: utf-8 -*-
"""
Created on Thu Aug  1 06:55:35 2019

@author: rever
"""
import csv
import htmls

url = "dates.csv"
liste = []

with open(url, encoding='utf-8-sig') as f:
    reader = csv.DictReader(f, delimiter = ';')
    for row in reader :
        liste.append(dict(row))
        
i = 0

html = htmls.debut

for ligne in liste :
    if i%2 == 0 :
        html += htmls.gauche % (ligne["lien"],
                                ligne["titre"],
                                ligne["date"],
                                ligne["texte"],
                                ligne["image"],
                                ligne["caption"],
                                ligne["caption"])
    else :
        html += htmls.droite % (ligne["image"],
                                ligne["caption"],
                                ligne["caption"],
                                ligne["lien"],
                                ligne["titre"],
                                ligne["date"],
                                ligne["texte"])
    i += 1

html += htmls.fin        

with open("histoire.html", 'w', encoding='utf-8') as f :
    f.write(html)
